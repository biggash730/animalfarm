class CreateStocks < ActiveRecord::Migration
  def change
    create_table :stocks do |t|
      t.integer :count
      t.references :breed, index: true

      t.timestamps
    end
  end
end
