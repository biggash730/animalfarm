class AddIsAdditionToIncidents < ActiveRecord::Migration
  def change
    add_column :incidents, :is_addition, :boolean
  end
end
