class CreateActivities < ActiveRecord::Migration
  def change
    create_table :activities do |t|
      t.integer :count
      t.datetime :date
      t.references :incident, index: true
      t.references :breed, index: true

      t.timestamps
    end
  end
end
