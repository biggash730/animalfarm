# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140606114702) do

  create_table "activities", force: true do |t|
    t.integer  "count"
    t.datetime "date"
    t.integer  "incident_id"
    t.integer  "breed_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "activities", ["breed_id"], name: "index_activities_on_breed_id"
  add_index "activities", ["incident_id"], name: "index_activities_on_incident_id"

  create_table "animals", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "breeds", force: true do |t|
    t.string   "name"
    t.integer  "animal_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "breeds", ["animal_id"], name: "index_breeds_on_animal_id"

  create_table "incidents", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "is_addition"
  end

  create_table "stocks", force: true do |t|
    t.integer  "count"
    t.integer  "breed_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "stocks", ["breed_id"], name: "index_stocks_on_breed_id"

end
