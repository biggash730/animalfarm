class IncidentsController < ApplicationController
	def new
	end

	def create
  		@incident = Incident.new(incident_params)
 
  		if @incident.save
    		redirect_to @incident
  		else
    		render 'new'
  		end
	end

	def show
  		@incident = Incident.find(params[:id])
	end

	def index
  		@incidents = Incident.all
	end

	def edit
  		@incident = Incident.find(params[:id])
	end

	def update
  		@incident = Incident.find(params[:id])
 
  		if @incident.update(incident_params)
    		redirect_to @incident
  		else
    	render 'edit'
  		end
	end

	def destroy
  		@incident = Incident.find(params[:id])
  		@incident.destroy
 
  		redirect_to incidents_path
	end

	private
  		def incident_params
    		params.require(:incident).permit(:name, :description, :is_addition)
  		end
end
