class BreedsController < ApplicationController
	before_action :set_record, only: [:show, :edit, :update, :destroy]

	def new
		@animals = Animal.all
	end

	def create
		@breed = Breed.new(breed_params)
 
  		if @breed.save
  			create_stock_record
    		redirect_to @breed
  		else
    		render 'new'
  		end
	end

	def show
	end

	def index
  		@breeds = Breed.all
	end

	def edit
  		@animals = Animal.all
	end

	def update
  		if @breed.update(breed_params)
  			create_stock_record
    		redirect_to @breed
  		else
    		render 'edit'
  		end
	end

	def destroy
  		@breed.destroy
 
  		redirect_to breeds_path
	end

	private
		def set_record
  			@breed = Breed.find(params[:id])
		end

  		def breed_params
    		params.require(:breed).permit(:name, :animal_id)
  		end

  		def create_stock_record
  			Stock.where(breed: @breed).first_or_create(count: 0)
  		end

end
