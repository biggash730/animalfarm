class ActivitiesController < ApplicationController
	before_action :set_record, only: [:show, :edit, :update, :destroy]

	def new
		@breeds = Breed.all
		@incidents = Incident.all
    @animals = Animal.all
	end

	def create
		@activity = Activity.new(activity_params)
 
  		if @activity.save
  			update_stock_record(@activity)
    		redirect_to '/stocks'
  		else
    		render 'new'
  		end
	end

	def show
	end

	def index
  		@activities = Activity.all
	end

	def edit
  		@breeds = Breed.all
		@incidents = Incident.all
	end

	def update
  		if @breed.update(breed_params)
  			create_stock_record
    		redirect_to @breed
  		else
    		render 'edit'
  		end
	end

	def destroy
  		@activity.destroy
  		update_stock_record(@activity, true)
  		redirect_to breeds_path
	end

	private
		def set_record
  			@activity = Activity.find(params[:id])
		end

  		def activity_params
    		params.require(:activity).permit(:count, :date, :breed_id, :incident_id)
  		end

  		def update_stock_record(activity, reverse = false)
  			if stock = activity.breed.stock
  				if (activity.incident.is_addition && !reverse) || (!activity.incident.is_addition && reverse)
            stock.increment(:count, activity.count)
          else
            stock.decrement(:count, activity.count)
          end
          # Negative stocks are allowed because of untimely transactions
          stock.save
  			end
  		end
end
