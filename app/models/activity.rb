# == Schema Information
#
# Table name: activities
#
#  id          :integer          not null, primary key
#  count       :integer
#  date        :datetime
#  incident_id :integer
#  breed_id    :integer
#  created_at  :datetime
#  updated_at  :datetime
#

class Activity < ActiveRecord::Base
  belongs_to :incident
  belongs_to :breed
end
