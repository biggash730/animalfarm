# == Schema Information
#
# Table name: stocks
#
#  id         :integer          not null, primary key
#  count      :integer
#  breed_id   :integer
#  created_at :datetime
#  updated_at :datetime
#

class Stock < ActiveRecord::Base
  belongs_to :breed
end
