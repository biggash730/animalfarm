# == Schema Information
#
# Table name: animals
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  description :text
#  created_at  :datetime
#  updated_at  :datetime
#

class Animal < ActiveRecord::Base
	has_many :breeds
	validates :name, presence: true
end
