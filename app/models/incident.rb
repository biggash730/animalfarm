# == Schema Information
#
# Table name: incidents
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  description :text
#  created_at  :datetime
#  updated_at  :datetime
#  is_addition :boolean
#

class Incident < ActiveRecord::Base
	belongs_to :activity
	validates :name, presence: true

	
end
